<?php

namespace App;

use App\Camp;
use App\Registration;
use Carbon\Carbon;
use App\Services\TenantManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Financials extends Model
{

    public function __construct()
    {

    }

    public function scopeTotalRevenueYTD()
    {
        
        $totalRevenueYTD = DB::table('registrations')
                         ->leftJoin('camps', 'camps.id', '=', 'registrations.camp_id')
                         ->leftJoin('tenants', 'camps.tenant_id', '=', 'tenants.id')
                         ->where('tenants.id', auth()->user()->tenant_id)
                         ->whereYear('registrations.created_at',Carbon::now()->format('Y'))
                         ->sum('price');
        return $totalRevenueYTD;
    }

    public function scopeTotalSignupsYTD()
    {
        
        $totalSignupsYTD = DB::table('registrations')
                         ->leftJoin('camps', 'camps.id', '=', 'registrations.camp_id')
                         ->leftJoin('tenants', 'camps.tenant_id', '=', 'tenants.id')
                         ->where('tenants.id', auth()->user()->tenant_id)
                         ->whereYear('registrations.created_at',Carbon::now()->format('Y'))
                         ->count('registrations.id');
        return $totalSignupsYTD;
    }

    public function scopeNewSignups($query, $days)
    {
        $newSignups = DB::table('registrations')
                         ->leftJoin('camps', 'camps.id', '=', 'registrations.camp_id')
                         ->leftJoin('tenants', 'camps.tenant_id', '=', 'tenants.id')
                         ->where('tenants.id', auth()->user()->tenant_id)
                         ->where('registrations.created_at', '>=', Carbon::now()->subDay($days))
                         ->count('registrations.id');
        return $newSignups;
    }

    public function scopeTopCampsByRevenue($query, $max = 3)
    {
        
        $totalRevenue = DB::table('registrations')
                         ->leftJoin('camps', 'camps.id', '=', 'registrations.camp_id')
                         ->leftJoin('tenants', 'camps.tenant_id', '=', 'tenants.id')
                         ->where('tenants.id', auth()->user()->tenant_id)
                         ->whereYear('registrations.created_at',Carbon::now()->format('Y'))
                         ->groupBy('registrations.camp_id')
                         ->limit($max)
                         ->select('camps.title', DB::raw('SUM(registrations.price) as price'), 'registrations.camp_id')
                         ->orderBy('price','desc')
                         ->get();
        return $totalRevenue;
    }

    public function scopeTopCampsByRegistrations($query, $max = 3)
    {
        
        $totalRegistrations = DB::table('registrations')
                            ->leftJoin('camps', 'camps.id', '=', 'registrations.camp_id')
                            ->leftJoin('tenants', 'camps.tenant_id', '=', 'tenants.id')
                            ->where('tenants.id', auth()->user()->tenant_id)
                            ->whereYear('registrations.created_at',Carbon::now()->format('Y'))
                            ->groupBy('registrations.camp_id')
                            ->limit($max)
                            ->select('camps.title', DB::raw('count(registrations.camp_id) as total'))
                            ->orderBy('total','desc')
                            ->get();
        return $totalRegistrations;
    }

}