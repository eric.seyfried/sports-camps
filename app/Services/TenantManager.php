<?php
namespace App\Services;
use App\Tenant;

class TenantManager {
    /*
     * @var null|App\Tenant
     */
    private $tenant;

    public function setTenant(?Tenant $tenant) {
        $this->tenant = $tenant;
        return $this;
    }
    
    public function getTenant(): ?Tenant {
        return $this->tenant;
    }
    
    public function loadTenant(string $identifier) 
    {
        $tenant = Tenant::query()->where('slug', '=', $identifier)->first();
        
        if ($tenant) {
            return $this->setTenant($tenant);
            // return true;
        }
        
        return false;
    }

    public function getSlug()
    {
        return $this->getTenant()->slug;
    }

    public static function unique($table, $column = 'NULL')
    {
        return (new Rules\Unique($table, $column))->where('tenant_id', $self->getTenant()->id);
    }

    public static function exists($table, $column = 'NULL')
    {
        return (new Rules\Exists($table, $column))->where('tenant_id', $self->getTenant()->id);
    }
 }