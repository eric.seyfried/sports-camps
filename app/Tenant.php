<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Tenant extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tenants';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['slug','name'];


    /**
     * Get the users for the tenant.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    /**
     * Boot function for using with Events
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->generateSlug();
        });
    }

    private function generateSlug()
    {
        $this->attributes['slug'] = Str::slug($this->name,'-');
    }

}
