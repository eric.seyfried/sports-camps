<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Traits\ModelHelpers;
use App\Services\TenantManager;

class Camp extends Model
{
    use SoftDeletes, ModelHelpers;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'title',
        'summary',
        'description',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'regular_price',
        'early_price',
        'early_price_starts',
        'early_price_ends',
        'early_price_email_on',
        'early_price_email_start_days',
        'early_price_email_end_days',
        'late_price',
        'late_price_starts',
        'late_price_ends',
        'late_price_email_on',
        'late_price_email_start_days',
        'late_price_email_end_days',
        'camp_start_date',
        'camp_end_date',
        'camp_start_time',
        'camp_end_time',
    ];

    public function scopeActive($query)
    {
        return $query->where('tenant_id', auth()->user()->tenant_id)
                     ->where('camp_end_date', '>', Carbon::now()->format('Y-m-d'))
                     ->orderBy('camp_start_date','asc');
    }

    public function scopeEverything($query)
    {
        return $query->where('tenant_id', auth()->user()->tenant_id)
                    //  ->where('camp_end_date', '>', Carbon::now()->format('Y-m-d'))
                     ->orderBy('camp_start_date','asc');
    }

    public function scopeExpired($query)
    {
        return $query->where('camp_end_date', '<', Carbon::now()->format('Y-m-d'));
    }

    public function totalRevenue()
    {
        return $this->registration->whereIn('status',['paid', 'pending'])->sum('price');
    }

    public function totalRefunds()
    {
        return $this->registration->whereIn('status',['cancelled'])->sum('price');
    }

    public function totalCancelled()
    {
        return $this->registration->whereIn('status',['cancelled'])->count();
    }

    public function totalRegistrations()
    {
        return $this->registration->whereIn('status',['paid', 'pending'])->count();
    }

    /**
     * get the current price of the camp based on the current date
     * @return float early_price, regular_price or late_price based on start/end dates
     */
    public function currentPrice()
    {
        $now = Carbon::now();
        $this->useRawDates();
        if ($now->between(Carbon::parse($this->early_price_starts), Carbon::parse($this->early_price_ends))) {
            // turn off 
            $this->useRawDates(false);
            return $this->early_price;
        }
        $this->useRawDates();
        if ($now->between(Carbon::parse($this->late_price_starts), Carbon::parse($this->late_price_ends))) {
            // turn off 
            $this->useRawDates(false);
            return $this->late_price;
        }
        // turn off 
        $this->useRawDates(false);
        return $this->regular_price;
    }

    public function isActive()
    {
        $now = Carbon::now();
        $this->useRawDates();
        $is_active = $now->between(Carbon::parse($this->camp_start_date), Carbon::parse($this->camp_end_date));
        // turn off 
        $this->useRawDates(false);
        return $is_active;
    }

    // mutators for auto-formatting
    public function setCampStartDateAttribute($value) {
        $this->attributes['camp_start_date'] = $this->settterDateFormatter($value);
    }
    
    public function setCampEndDateAttribute($value) {
        $this->attributes['camp_end_date'] = $this->settterDateFormatter($value);
    }

    public function setEarlyPriceStartsAttribute($value) {
        $this->attributes['early_price_starts'] = $this->settterDateFormatter($value);
    }
    
    public function setEarlyPriceEndsAttribute($value) {
        $this->attributes['early_price_ends'] = $this->settterDateFormatter($value);
    }

    public function setLatePriceStartsAttribute($value) {
        $this->attributes['late_price_starts'] = $this->settterDateFormatter($value);
    }
    
    public function setLatePriceEndsAttribute($value) {
        $this->attributes['late_price_ends'] = $this->settterDateFormatter($value);
    }

    public function setCampStartTimeAttribute($value) {
        $this->attributes['camp_start_time'] = $this->settterTimeFormatter($value);
    }
    
    public function setCampEndTimeAttribute($value) {
        $this->attributes['camp_end_time'] = $this->settterTimeFormatter($value);
    }

    public function setRegularPriceAttribute($value) {
        $this->attributes['regular_price'] = clean_price($value);
    }

    public function setEarlyPriceAttribute($value) {
        $this->attributes['early_price'] = clean_price($value);
    }

    public function setLatePriceAttribute($value) {
        $this->attributes['late_price'] = clean_price($value);
    }


    // accessor mutators

    public function getCampStartDateAttribute($value) {
        return $this->getterDateFormatter($value);
    }

    public function getCampEndDateAttribute($value) {
        return $this->getterDateFormatter($value);
    }

    public function getEarlyPriceStartsAttribute($value) {
        return $this->getterDateFormatter($value);
    }
    
    public function getEarlyPriceEndsAttribute($value) {
        return $this->getterDateFormatter($value);
    }

    public function getLatePriceStartsAttribute($value) {
        return $this->getterDateFormatter($value);
    }
    
    public function getLatePriceEndsAttribute($value) {
        return $this->getterDateFormatter($value);
    }

    public function getCampStartTimeAttribute($value) {
        return $this->getterTimeFormatter($value);
    }
    
    public function getCampEndTimeAttribute($value) {
        return $this->getterTimeFormatter($value);
    }

    
    // relationships

    public function tenant()
    {
        return $this->belongsTo('App\Tenant','tenant_id', 'id');
    }

    public function registration()
    {
        return $this->hasMany('App\Registration','camp_id', 'id');
    }

    public function emailTemplates()
    {
        return $this->hasMany('App\EmailTemplate','camp_id', 'id');
    }
}
