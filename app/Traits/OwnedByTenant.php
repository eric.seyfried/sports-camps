<?php
namespace App\Traits;

use App\Scopes\TenantOwnedScope;

trait OwnedByTenant {
    public static function bootOwnedByTenant() {
        static::addGlobalScope(new TenantOwnedScope);
        
        static::creating(function ($model) {
            if (! $model->tenant_id && ! $model->relationLoaded('tenant')) {
                $model->setRelation('tenant', app(TenantManager::class)->getTenant());
            }
            
            return $model;
        }
    }
    
    public function tenant() {
        $this->belongsTo(Tenant::class, 'tenant_id');
    }
}