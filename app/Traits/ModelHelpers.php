<?php
namespace App\Traits;
use Carbon\Carbon;

trait ModelHelpers {

    private $use_raw_dates = false;

    /**
     * use raw dates from database without transformation
     */
    public function useRawDates($on = true)
    {
        $this->use_raw_dates = $on;
    }

    public function settterDateFormatter($value)
    {
        return Carbon::createFromFormat('m-d-Y', $value)->format('Y-m-d');
    }

    public function settterTimeFormatter($value)
    {
        return Carbon::createFromFormat('h:i A', $value)->format('H:i:s');
    }

    public function getterDateFormatter($value)
    {
        if ($this->use_raw_dates || empty($value)) {
            return $value;
        }
        
        return Carbon::createFromFormat('Y-m-d', $value)->format('m-d-Y');
    }

    public function getterTimeFormatter($value)
    {
        if ($this->use_raw || empty($value)) {
            return $value;
        }
        return Carbon::createFromFormat('H:i:s', $value)->format('h:i A');
    }
}