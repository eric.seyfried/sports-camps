<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class EmailTemplate extends Model
{
    use SoftDeletes, ModelHelpers;

    public $timestamps = true;

    public function scopeTemplateType($query, $template_type)
    {
        // use optional to return default template type if camp template cannot be found
        return optional($query->where('template_type', $template_type), function($query) use($template_type) {
            // camp template is null
            if (is_null($query->first())) {
                // query, set the Model instance to hydrate and return
                $eloquent_builder = new Builder(
                    DB::table('email_templates')
                        ->where('template_type', $template_type)
                        ->where('is_default', true)
                );
                $eloquent_builder->setModel(new EmailTemplate());
                return $eloquent_builder->get();
            }
            // camp template is found
            return $query;
        });
    }

    public function scopeHasTemplateType($query, $template_type)
    {
        $result = $query->where('template_type', $template_type)->first();
        return is_null($result) ? false : true;
    }

    public function camp()
    {
        return $this->belongsTo('App\Camp','id', 'camp_id');
    }
}
