<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Traits\ModelHelpers;

class Registration extends Model
{
    use SoftDeletes, ModelHelpers;

    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'camp_id',
        'user_id',
        'price',
        'stripe_id',
        'status',
    ];

    public function getCreatedAtAttribute($value) {
        return $this->getterDateFormatter(Carbon::parse($value)->format('Y-m-d'));
    }

    // relationships

    public function user()
    {
        return $this->belongsTo('App\User','user_id', 'id');
    }

    public function camp()
    {
        return $this->belongsTo('App\Camp','camp_id', 'id');
    }

    
}
