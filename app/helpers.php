<?php
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Broadcasting\Factory as BroadcastFactory;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Cookie\Factory as CookieFactory;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Foundation\Mix;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Queue\CallQueuedClosure;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\HtmlString;
use Symfony\Component\HttpFoundation\Response;
use \App\Services\TenantManager;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

if (! function_exists('tenant_action')) {
    /**
     * Generate the tenant based URL to a controller action.
     *
     * @param  string|array  $name
     * @param  mixed  $parameters
     * @param  bool  $absolute
     * @return string
     */
    function tenant_action($name, $parameters = [], $absolute = true)
    {
        $tenant = app(TenantManager::class);
        $parameters = array_merge([$tenant->getTenant()->slug], $parameters);
        return app('url')->action($name, $parameters, $absolute);
    }
}

if (! function_exists('tenant_route')) {
    /**
     * Generate the tenant based URL to a named route.
     *
     * @param  array|string  $name
     * @param  mixed  $parameters
     * @param  bool  $absolute
     * @return string
     */
    function tenant_route($name, $parameters = [], $absolute = true)
    {
        $tenant = app(TenantManager::class);
        // $tenant_route = $tenant->getTenant()->slug . '/' . $name;
        // $route = Str::replaceFirst($name,$tenant_route, app('url')->route($name, $parameters, $absolute));
        $parameters = array_merge(
            [$tenant->getTenant()->slug],
            $parameters
        );
        return app('url')->route($name, $parameters, $absolute);
    }
}

if (! function_exists('tenant_redirect')) {
    /**
     * Get an instance of the redirector.
     *
     * @param  string|null  $to
     * @param  int  $status
     * @param  array  $headers
     * @param  bool|null  $secure
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function tenant_redirect($to = null, $status = 302, $headers = [], $secure = null)
    {
        if (is_null($to)) {
            return app('redirect');
        }
        
        $tenant = app(TenantManager::class);
        if ($tenant->getTenant()) {
            Log::debug(__METHOD__ , ['slug' => $tenant->getTenant()->slug]);
            $tenant_redirect = $tenant->getTenant()->slug . '/' . Str::replaceFirst('/', '', $to);
            $redirect = Str::replaceFirst($to, $tenant_redirect, $to);
            $to = $redirect;
        }
        return app('redirect')->to($to, $status, $headers, $secure);
    }
}

if (! function_exists('tenant_url')) {
    /**
     * Get tenant_url for tenant.
     *
     * @param  string|null  $to
     * @param  int  $status
     * @param  array  $headers
     * @param  bool|null  $secure
     * @return str
     */
    function tenant_url($to = null)
    {
        if (is_null($to)) {
            return null;
        }
        $tenant = app(TenantManager::class);
        if ($tenant->getTenant()) {
            $url = $tenant->getTenant()->slug . '/' . Str::replaceFirst('/', '', $to);
            $url = Str::replaceFirst($to, $url, $to);
            $to = $url;
        }
        return $to;
    }
}

if (! function_exists('make_float')) {
    /**
     * convert an integer to a float by dividing by 100
     *
     * @param  int  $integer
     * @return float
     */
    function make_float(int $integer)
    {
        return $integer / 100;
    }

}

if (! function_exists('from_float')) {
    /**
     * convert a float to a integer by multiplying by 100
     *
     * @param  float  $float
     * @return integer
     */
    function from_float(float $float)
    {
        return $float * 100;
    }
}

if (! function_exists('clean_price')) {
    /**
     * strips $ and , from a price value
     *
     * @param  string  $price
     * @return string
     */
    function clean_price($price)
    {
        return preg_replace('/[$,]/', '', $price);
    }
}

if (! function_exists('elipsis')) {
    /**
     * trim string to passed length and append char
     *
     * @param  string  $string
     * @param integer $length = 100
     * @param string $char = ...
     * @return string
     */
    function elipsis($string, $length = 100, $char = '...')
    {
        return trim(substr($string, 0, $length)) . $char;
    }
}

if (! function_exists('money')) {
    /**
     * return a formatted money string
     *
     * @param  string  $value
     * @return string
     */
    function money($value)
    {
        return '$' . number_format($value, 2);
    }
}

if (! function_exists('format_date')) {
    /**
     * return a formatted date string using Carbon
     *
     * @param  string  $value
     * @param  string  $format new format for date
     * @param  string  $fromFormat original format of date. default m-d-Y
     * @return string
     */
    function format_date($value, $format, $fromFormat = 'm-d-Y')
    {
        return Carbon::createFromFormat($fromFormat, $value)->format($format);
    }
}