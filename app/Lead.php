<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Traits\ModelHelpers;
use App\Services\TenantManager;

class Lead extends Model
{
    use SoftDeletes, ModelHelpers;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'first_name',
        'last_name',
        'email',
        'gender',
        'birthdate',
    ];


    public function scopeAge($query)
    {
        $this->useRawDates();
        $age = Carbon::parse($this->birthdate)->age;
        $this->useRawDates(false);
        return $age;
    }

    // accessors

    public function getBirthdateAttribute($value) {
        return $this->getterDateFormatter($value);
    }

    // mutators for auto-formatting
    public function setBirthdateAttribute($value) {
        $this->attributes['birthdate'] = $this->settterDateFormatter($value);
    }

    public function tenant()
    {
        return $this->belongsTo('App\Tenant','tenant_id', 'id');
    }
}
