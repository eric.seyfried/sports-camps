<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCampRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'summary' => 'required|max:255',
            'description' => 'required|max:1000',
            'regular_price' => 'required|regex:' . $this->currencyRule(),
            'camp_start_date' => 'required|date_format:m-d-Y',
            'camp_end_date' => 'required|date_format:m-d-Y',
            'camp_start_time' => 'required',
            'camp_end_time' => 'required',
            'early_price' => 'required|regex:' . $this->currencyRule(),
            'early_price_starts' => 'required_with:early_price|date_format:m-d-Y',
            'early_price_ends' => 'required_with:early_price|date_format:m-d-Y',
            'late_price' => 'required|regex:' . $this->currencyRule(),
            'late_price_starts' => 'required_with:early_price|date_format:m-d-Y',
            'late_price_ends' => 'required_with:early_price|date_format:m-d-Y',
            'regular_price' => 'required|regex:' . $this->currencyRule(),
            'address_1' => 'required_with:city,state,zip|max:100|regex:' . $this->charsRule(),
            'address_2' => 'max:100|regex:' . $this->charsRule(),
            'city' => 'required_with:address_1,state,zip|max:50|regex:/^[a-z ]$/i',
            'state' => 'required_with:address_1,city,zip|regex:/^[a-z]{2,2}$/i',
            'zip' => 'required_with:address_1,city,state|digits:7',
        ];
    }

    private function currencyRule()
    {
        return '/^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/';
    }

    private function charsRule()
    {
        return '/^[a-z0-9 ,-\.#]$/i';
    }
}
