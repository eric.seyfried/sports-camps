<?php

namespace App\Http\Controllers;

use App\Camp;
use App\Lead;
use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCampRequest;


class CampController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $include_expired = $request->input('include_expired',false);
        $data = [
            'camps' => ($include_expired) ? Camp::everything()->get() : Camp::active()->get(),
            'include_expired' => $include_expired
        ];
        if (count($data['camps']) == 0) {
            return view('getting-started');
        }
        return View('camp.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('camp.create', ['camp' => Camp::make()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateCampRequest  $CreateCampRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCampRequest $CreateCampRequest)
    {
        $camp = Camp::make(
            $CreateCampRequest->only(
                ['title',
                'summary',
                'description',
                'camp_start_date',
                'camp_end_date',
                'camp_start_time',
                'camp_end_time',
                'early_price',
                'early_price_starts',
                'early_price_ends',
                'regular_price',
                'late_price',
                'late_price_starts',
                'late_price_ends',
                'address_1',
                'address_2',
                'city',
                'state',
                'zip',
            ])
        );
        $camp->tenant_id = auth()->user()->tenant_id;
        $camp->save();
        dd();
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id camp id
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request, $id)
    {
        $camp = Camp::findOrFail($id);
        $leads = Lead::make();
        $filter = $request->input('filter', null);
        $data = [
            'camp' => $camp,
            'leads' => null,
            'active' => $request->input('active', is_null($filter) ? 'summary' : 'registrants'),
            'filter' => $filter,
        ];
        return View('camp.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id camp id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request, $id)
    {
        $camp = Camp::findOrFail($id);
        $data = [
            'camp' => $camp
        ];
        return View('camp.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CreateCampRequest  $CreateCampRequest
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCampRequest $CreateCampRequest)
    {
        $camp = Camp::findOrFail($CreateCampRequest->id);
        $camp->fill(
            $CreateCampRequest->only(
                ['title',
                'summary',
                'description',
                'camp_start_date',
                'camp_end_date',
                'camp_start_time',
                'camp_end_time',
                'early_price',
                'early_price_starts',
                'early_price_ends',
                'regular_price',
                'late_price',
                'late_price_starts',
                'late_price_ends',
                'address_1',
                'address_2',
                'city',
                'state',
                'zip',
            ])
        );
        $camp->save();
        return redirect('camp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id camp id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request, $id)
    {
        $camp = Camp::findOrFail($request->id);
        $camp->delete();
        return tenant_redirect('camp');
    }

    public function saveEmailSettings(Request $request)
    {
        $camp = Camp::findOrFail($request->id);
        
        // if camp has email template, update it
        if ($camp->emailTemplates()->hasTemplateType('early_registration')) {
            $email_template = $camp->emailTemplates()->find($request->early_registration_email_template_id);
        }
        // otherwise, make a new one
        else {
            $email_template = new EmailTemplate();
            $email_template->camp_id = $camp->id;
            $email_template->template_type = 'early_registration';
            $email_template->is_default = false;

        }
        $email_template->subject = $request->early_registration_email_subject;
        $email_template->body = $request->early_registration_email_body;
        $email_template->save();

        // if camp has email template, update it
        if ($camp->emailTemplates()->hasTemplateType('late_registration')) {
            $email_template = $camp->emailTemplates()->find($request->late_registration_email_template_id);
        }
        // otherwise, make a new one
        else {
            $email_template = new EmailTemplate();
            $email_template->camp_id = $camp->id;
            $email_template->template_type = 'late_registration';
            $email_template->is_default = false;

        }
        $email_template->subject = $request->late_registration_email_subject;
        $email_template->body = $request->late_registration_email_body;
        $email_template->save();
        
        // update camp email settings
        $camp->early_price_email_on = $request->has('enable_early_registration_email_starts');
        $camp->early_price_email_start_days = $request->input('early_price_email_start_days', 3);
        $camp->early_price_email_end_days = $request->input('early_price_email_end_days', 3);

        $camp->late_price_email_on = $request->has('enable_late_registration_email_starts');
        $camp->late_price_email_start_days = $request->input('late_price_email_start_days', 3);
        $camp->late_price_email_end_days = $request->input('late_price_email_end_days', 3);
        $camp->save();

        return redirect(
                tenant_route('tenant:camp.show', [$camp->id, 'active' => 'email'], false)
        );
    }

    public function searchCampaign(Request $request)
    {
        
        $leads = Lead::when($request->input('gender'), function ($query, $gender) {
                    return $query->whereIn('gender', array_values($gender));
                })->when($request->input('include_existing'), function ($query, $include_existing) {
                    return $query;
                })->when($request->input('age_min'), function ($query, $age_min) {
                    return $query->whereRaw("YEAR(curdate())-YEAR(birthdate) >= " . $age_min);
                })
                ->when($request->input('age_max'), function ($query, $age_max) {
                    return $query->whereRaw("YEAR(curdate())-YEAR(birthdate) <= " . $age_max);
                })
                ->orderBy('last_name', 'asc')
                ->get();
        
        $camp = Camp::findOrFail($request->id);

        $filter = $request->input('filter', null);
        $data = [
            'camp' => $camp,
            'leads' => $leads,
            'active' => 'campaign',
            'filter' => $filter,
            'leads' => $leads,
        ];
        return View('camp.show', $data);

    }

}
