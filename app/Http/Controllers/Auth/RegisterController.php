<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Tenant;
use App\Plan;
use App\Services\TenantManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Facades\TenantManager as TenantManagerFacade;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
        if (is_null($data['tenant_id'])) {
            $rules['tenant_name'] = ['required', 'string', 'max:255'];
        }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = null;
        DB::transaction(function() use ($data, &$user)
        {
            /**
             * create tenant user
             */
            if (is_null($data['tenant_id'])) {
                $tenant = Tenant::firstOrCreate([
                    'name' => $data['tenant_name'],
                ]);
                $user_type = 'tenant';
            }
            /**
             * or create customer user for a given tenant
             */
            else {
                $tenant = Tenant::findOrFail($data['tenant_id']);
                $user_type = 'customer';
            }
            
            $user = User::create([
                'name' => $data['name'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'tenant_id' => $tenant->id,
                'user_type' => $user_type
            ]);

            // set the tenant slug
            app(TenantManager::class)->loadTenant($tenant->slug);
        
            Log::info('user created', array_merge(['user_type' => $user_type], $data));
            // get plan data
            $plan = Plan::find($data['plan']);
            Log::info('payment created', [
                'plan' => $plan->name, 
                'plan_id' => $plan->id,
                'amount' => $plan->amount,
                'strip_plan_id' => $plan->stripe_plan_id, 
                'stripeToken' => $data['stripeToken']
            ]);
            if ($user_type == 'tenant') {
                // create new subscription
                $user->newSubscription('Sports Camp Manager', $plan->stripe_plan_id)->create($data['stripeToken']);
            }
            else {
                // create one time charge
                $stripeCharge = $user->charge(
                    from_float($plan->amount), 
                    $data['stripeToken'],
                    [
                        'description' => $plan->name,
                        'metadata' => [
                            'user_id' => $user->id,
                            'tenant_id' => $tenant->id,
                        ]
                    ]
                );
                Log::info('payment made', 
                    [
                        'payment_id' => $stripeCharge->id,
                        'status' => $stripeCharge->status,
                        'created_at' => $stripeCharge->created,
                    ]
                );
            }
            

        });
        // end transaction
        

        return $user;
    }

    protected function index()
    {
        // $plans = Plan::all();
        $data = [
            'plans' => Plan::all()
        ];
        return view('register.landing', $data);
    }

    public function showRegistrationForm()
    {
        $user = new User();
        // $tenantManager = app(TenantManager::class)->loadTenant($tenant->slug);
        // $tenant = app(TenantManager::class)->getTenant();
        $tenant = TenantManagerFacade::getTenant();
        $plan = Plan::find(request()->get('plan'));
        return view('auth.register', 
                [
                    'plan' => $plan,
                    'intent' => $user->createSetupIntent(),
                    'tenant' => $tenant,
                    'name' => $plan->name,
                    'amount' => $plan->amount
                ]
            );
    }

    /**
     * return the tenant prefixed url path
     * @return string
     */
    public function redirectTo()
    {
        return tenant_url($this->redirectTo);
    }
}