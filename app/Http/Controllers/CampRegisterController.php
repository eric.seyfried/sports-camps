<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Tenant;
use App\Plan;
use App\Services\TenantManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Facades\TenantManager as TenantManagerFacade;
use App\Camp;
use App\Registration;

class CampRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Camp Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request, $camp_id)
    {

        // $rules = [
        //     'title' => ['required', 'string', 'max:100'],
            
        // ];
        // if (is_null($data['tenant_id'])) {
        //     $rules['tenant_name'] = ['required', 'string', 'max:255'];
        // }

        // return Validator::make($data, $rules);
        return true;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  integer  $camp_id
     * @return \App\User
     */
    protected function checkout(Request $request, $camp_id)
    {
        $user = null;
        DB::transaction(function() use ($request, &$user, $camp_id)
        {

            $tenant = Tenant::findOrFail($request->tenant_id);
            $camp = Camp::findOrFail($camp_id);
            $user_type = 'customer';
            // set the tenant slug
            app(TenantManager::class)->loadTenant($tenant->slug);
            
            
            $user = User::create([
                'name' => $request->name,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'tenant_id' => $tenant->id,
                'user_type' => $user_type
            ]);

            Log::info('user created', [
                'user_type' => $user_type,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'email' => $user->email
            ]);

            $registration = Registration::create([
                'camp_id' => $camp_id,
                'user_id' => $user->id,
                'stripe_id' => $request->stripeToken,
                'status' => 'pending',
                'price' => $camp->currentPrice(),
            ]);

            Log::info('registration created', [
                'camp_id' => $camp->id, 
                'user_id' => $user->id,
                'price' => $camp->currentPrice(),
                'stripeToken' => $request->stripeToken
            ]);
            
            // create one time charge
            $stripeCharge = $user->charge(
                from_float($camp->currentPrice()), 
                $request->stripeToken,
                [
                    'description' => $camp->title,
                    'metadata' => [
                        'user_id' => $user->id,
                        'camp_id' => $camp->id,
                        'tenant_id' => $tenant->id,
                    ]
                ]
            );

            Log::info('payment made', 
                [
                    'payment_id' => $stripeCharge->id,
                    'status' => $stripeCharge->status,
                    'created_at' => $stripeCharge->created,
                ]
            );
            
            // update status to paid or cancelled, etc.
            switch ($stripeCharge->status) {
                case 'succeeded':
                    $registration->status = 'paid';
                    $registration->save();
                break;
            }
            

        });
        // end transaction
        

        return $user;
    }

    protected function index()
    {
        // $plans = Plan::all();
        $data = [
            'plans' => Plan::all()
        ];
        return view('register.landing', $data);
    }

    public function showRegistrationForm(Request $request, $id)
    {
        $user = new User();
        $camp = Camp::findOrFail($id);

        $data = [
            'camp' => $camp,
            'tenant' => app(TenantManager::class)->getTenant(),
            'intent' => $user->createSetupIntent(),
            'name' => '',//$camp->title,
            'amount' => $camp->currentPrice(),
        ];
        return View('camp.register', $data);
    }

    /**
     * return the tenant prefixed url path
     * @return string
     */
    public function redirectTo()
    {
        return tenant_url($this->redirectTo);
    }
}