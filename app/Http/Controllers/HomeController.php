<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TenantManager;
use App\Camp;
use App\Financials;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(TenantManager $tenant)
    {
        if (count(Camp::active()->get()) == 0) {
            return view('getting-started');
        }
        
        $data = [
            'total_revenue_ytd' => Financials::totalRevenueYTD(),
            'total_signups_ytd' => Financials::totalSignupsYTD(),
            'new_signups' => Financials::newSignups(7),
            'top_camps_by_revenue' => Financials::topCampsByRevenue(5),
            'top_camps_by_registrations' => Financials::topCampsByRegistrations(5),
        ];

        return view('home', $data);
    }
}
