<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        // create a tenant
        $tenant = factory(App\Tenant::class, 1)->create();
        $tenant_user = factory(App\User::class, 1)->create(['tenant_id' => $tenant[0]->id, 'user_type' => 'tenant']);
        $camps = factory(App\Camp::class, 5)->create(['tenant_id' => $tenant[0]->id]);
        $leads = factory(App\Lead::class, 50)->create(['tenant_id' => $tenant[0]->id]);
        $email_template = factory(App\EmailTemplate::class, 1)->create([
            'camp_id' => 0,
            'template_type' => 'early_registration',
            'is_default' => true
            ]);
        $email_template = factory(App\EmailTemplate::class, 1)->create([
            'camp_id' => 0,
            'template_type' => 'late_registration',
            'is_default' => true
            ]);
        

        // register the customers for the camps
        foreach($camps as $camp) {
            // create customers for the tenant
            $customer_users = factory(App\User::class, 10)->create(['tenant_id' => $tenant[0]->id, 'user_type' => 'customer']);  
            foreach($customer_users as $user) {
                $camp_registrations = factory(App\Registration::class, 1)->create(['camp_id' => $camp->id, 'user_id' => $user->id]);
            }
        }
        
    }
}
