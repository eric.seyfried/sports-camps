<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertStripeProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('plan')->insert(
            [
                'stripe_plan_id' => 'plan_GyZTa0PyktkbMT',
                'name' => 'Basic',
                'amount' => '9.99',
                'interval' => 'month',
                'interval_count' => '0',
                'trial_period_days' => '30',
            ]
        );
        DB::table('plan')->insert(
            [
                'stripe_plan_id' => 'plan_GyZl3vLQqVVOKz',
                'name' => 'Pro',
                'amount' => '14.99',
                'interval' => 'month',
                'interval_count' => '0',
                'trial_period_days' => '30',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
