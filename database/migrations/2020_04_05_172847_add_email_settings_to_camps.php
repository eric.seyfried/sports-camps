<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailSettingsToCamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('camps', function (Blueprint $table) {
            $table->boolean('early_price_email_on')->default(true)->after('early_price_ends');
            $table->smallInteger('early_price_email_start_days')->default(3)->after('early_price_email_on');
            $table->smallInteger('early_price_email_end_days')->default(3)->after('early_price_email_start_days');
            $table->boolean('late_price_email_on')->default(true)->after('late_price_ends');
            $table->smallInteger('late_price_email_start_days')->default(3)->after('late_price_email_on');
            $table->smallInteger('late_price_email_end_days')->default(3)->after('late_price_email_start_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('camps', function (Blueprint $table) {
            $table->dropColumn('early_price_email_on');
            $table->dropColumn('early_price_email_start_days');
            $table->dropColumn('early_price_email_end_days');
            $table->dropColumn('late_price_email_on');
            $table->dropColumn('late_price_email_start_days');
            $table->dropColumn('late_price_email_end_days');
        });
    }
}
