<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camps', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tenant_id');
            $table->text('title')->nullable();
            $table->text('summary')->nullable();
            $table->text('description')->nullable();
            $table->text('address_1')->nullable();
            $table->text('address_2')->nullable();
            $table->text('city')->nullable();
            $table->text('state')->nullable();
            $table->text('zip')->nullable();
            $table->decimal('regular_price',8, 2)->nullable();
            $table->decimal('early_price', 8, 2)->nullable();
            $table->date('early_price_starts')->nullable();
            $table->date('early_price_ends')->nullable();
            $table->decimal('late_price', 8, 2)->nullable();
            $table->date('late_price_starts')->nullable();
            $table->date('late_price_ends')->nullable();
            
            $table->date('camp_start_date')->nullable();
            $table->date('camp_end_date')->nullable();
            $table->time('camp_start_time')->nullable();
            $table->time('camp_end_time')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camps');
    }
}
