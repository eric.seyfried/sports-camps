<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Camp;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Camp::class, function (Faker $faker) {
    $start_date = Carbon::createFromDate(date('Y'), $faker->month(), $faker->dayOfMonth());
    $start_time = Carbon::createFromTime('10', '00', '00');
    $camp_titles = [
        'Basic Ground Balls/Scooping',
        'Catching, Passing, Shooting',
        'Blocking/Checking',
        'Elite Attack Skills',
        'Perfecting the Triangle Offense',
        'Parents Guide to Becoming a Lax Coach in 3 days',
    ];
    return [
        'title' => $faker->randomElement($camp_titles),
        'summary' => $faker->realText(200),
        'description' => join(PHP_EOL, [$faker->realText(100), $faker->realText(50), $faker->realText(100)]),
        'address_1' => join(' ', [$faker->buildingNumber, $faker->streetName]),
        'address_2' => $faker->secondaryAddress,
        'city' => $faker->city,
        'state' => $faker->stateAbbr,
        'zip' => $faker->postcode,
        'regular_price' => $faker->randomFloat(2, 79, 199),
        'early_price' => $faker->randomFloat(2, 79, 199),
        'late_price' => $faker->randomFloat(2, 79, 199),
        'early_price_starts' => $start_date->copy()->subDays(20)->format('m-d-Y'),
        'early_price_ends' => $start_date->copy()->subDays(15)->format('m-d-Y'),
        'late_price_starts' => $start_date->copy()->subDays(5)->format('m-d-Y'),
        'late_price_ends' => $start_date->copy()->subDays(1)->format('m-d-Y'),
        'camp_end_date' => $start_date->copy()->addDays($faker->numberBetween(3,19))->format('m-d-Y'),
        'camp_start_date' => $start_date->copy()->format('m-d-Y'),
        'camp_end_date' => $start_date->copy()->addDays($faker->numberBetween(3,19))->format('m-d-Y'),
        'camp_start_time' => $start_time->format('h:i A'),
        'camp_end_time' => $start_time->copy()->addHour($faker->numberBetween(1,2))->format('h:i A'),
    ];
});
