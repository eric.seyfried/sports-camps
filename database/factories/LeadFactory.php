<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lead;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Lead::class, function (Faker $faker, $tenant_id = null) {
    $gender = $faker->randomElement(['male','female']);
    return [
        'tenant_id' => $tenant_id, 
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'gender' => $gender,
        'birthdate' => $faker->dateTimeThisCentury->format('d-m-Y'),
    ];
});
