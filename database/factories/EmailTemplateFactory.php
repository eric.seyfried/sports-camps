<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmailTemplate;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(EmailTemplate::class, function (Faker $faker) {
    $body = ['Dear {first_name}'];
    $body[] = $faker->realText(100);
    $body[] = $faker->realText(20) . ' {camp_start_date} - {camp_end_date} ' . $faker->realText(20);
    $body[] = $faker->realText(100);
    $body[] = $faker->realText(50);
    $body[] = $faker->realText(50) . ' (early_price} starts on {early_price_starts}';
    return [
        'is_default' => false,
        'subject' => $faker->realText(100),
        'body' => join(PHP_EOL, $body),
        
    ];
});
