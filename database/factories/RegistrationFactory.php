<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Registration;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Registration::class, function (Faker $faker) {
    return [
        'status' => $faker->randomElement(['paid', 'pending', 'cancelled']),
        'price' => $faker->randomFloat(2, 79, 199),
        'stripe_id' => 'PM_' . $faker->numberBetween(1000, 9999),
    ];
});
