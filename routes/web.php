<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Auth\RegisterController@index')->name('landing');
// tenant registration
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');





Auth::routes();


Route::group([
    'prefix'     => '/{tenant}',
    'middleware' => \App\Http\Middleware\IdentifyTenant::class,
    'as'         => 'tenant:',
], function () {
    // Tenant routes here
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

    // camp routes
    Route::get('camp', 'CampController@index')->name('camp.list');
    Route::post('camp', 'CampController@index')->name('camp.list');
    Route::get('camp/show/{id}', 'CampController@show')->name('camp.show');
    Route::get('camp/edit/{id}', 'CampController@edit')->name('camp.edit');
    Route::get('camp/create', 'CampController@create')->name('camp.create');
    Route::post('camp/update', 'CampController@update')->name('camp.update');
    Route::get('camp/delete/{id}', 'CampController@destroy')->name('camp.delete');
    Route::post('camp/store', 'CampController@store')->name('camp.store');
    Route::post('camp/save-email/{id}', 'CampController@saveEmailSettings')->name('camp.email');
    Route::post('camp/campaign-search', 'CampController@searchCampaign')->name('camp.campaign-search');
    Route::post('camp/campaign-save', 'CampController@saveCampaign')->name('camp.campaign-save');

    // camp registration
    Route::get('register/{id}', 'CampRegisterController@showRegistrationForm')->name('camp.register');
    Route::post('register/{id}', 'CampRegisterController@checkout')->name('camp.checkout');
});