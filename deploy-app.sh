#!/bin/bash


if [ -d "/var/docker/laravel/.git" ]
then
        cd /var/docker/laravel
        echo "discarding changes to composer.lock"
        sudo git checkout -- composer.lock
        echo "git pull"
        sudo git pull && echo "sports-camps pulled"
        # run composer
        # sudo docker exec sports-camp-app /usr/local/bin/composer update
        sudo docker run --rm -v $(pwd):/app composer update --no-dev

        # run npm 
        # sudo docker exec -u 0 sports-camp-app npm install --global cross-env --production
        sudo docker exec -u 0 sports-camp-app npm install --global cross-env && echo "npm install cross-env complete"

        sudo docker exec -u 0 sports-camp-app npm install && echo "npm install complete"

        ## separate these calls as together, error out in npm cannot be found
        sudo docker exec -u 0 sports-camp-app npm run prod
else
        

        # /var/docker/laravel already will exist, stay one dir higher and replace it
        cd /var/docker
        sudo git clone https://gitlab.com/eric.seyfried/sports-camps.git laravel && echo "sports-camps cloned"

        # install all vendor libs (composer.lock is there so update not install cmd)
        cd /var/docker/laravel
        sudo docker run --rm -v $(pwd):/app composer update --no-dev

        # run npm 
        # sudo docker exec -u 0 sports-camp-app npm install --global cross-env --production
        sudo docker exec -u 0 sports-camp-app npm install --global cross-env && echo "npm install cross-env complete"

        sudo docker exec -u 0 sports-camp-app npm install && echo "npm install complete"

        ## separate these calls as together, error out in npm cannot be found
        sudo docker exec -u 0 sports-camp-app npm run prod
fi

# install dotenv script to manage .env files
if [ ! -d "/home/deployer/bin" ]
then
        echo "installing dotenv in ~bin"
        #su - deployer
        mkdir ~/bin
        cd ~/bin
        git clone https://github.com/bashup/dotenv.git
fi

# add environment variables to .env file
. /home/deployer/bin/dotenv/dotenv
echo "grabbing a copy of .env.example"
cp /var/docker/laravel/.env.example /home/deployer/.env.example
.env --file /home/deployer/.env.example

input="/home/deployer/.env-vars"
while IFS= read -r line
do
  .env set $line
done < "$input"


echo "copying .env.example -> /var/docker/laravel/.env"
sudo cp /home/deployer/.env.example /var/docker/laravel/.env
rm /home/deployer/.env.example

echo "setting owner of /var/docker/laravel/ to www"
sudo chown -R www:www /var/docker/laravel/

echo "Running Artisan"
sudo docker exec sports-camp-app php artisan key:generate && echo "artisan key:generate complete"
sudo docker exec sports-camp-app php artisan config:cache && echo "artisan config:cache complete"
sudo docker exec sports-camp-app php artisan migrate && echo "artisan migrate complete"