@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body text-center">
                    <h4 class="font-weight-bold">Welcome to {{ config('app.name', 'Laravel') }}!</h4>
                    <hr>
                    <p>Let's get you started! First, you'll need to create your first camp.</p>
                    <a href="{{ tenant_route('tenant:camp.create') }}" class="btn btn-primary">Create a Camp</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
