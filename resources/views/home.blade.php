@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Dashboard</h3>
    <div class="row sc-stats">
        <div class="col-md-4 col-sm-12 mb-4">
            <!-- Card -->
            <div class="card success-color lighten-1 white-text">
                <!-- Card content -->
                <div class="card-body">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ money(__($total_revenue_ytd)) }}</p>
                        <p class="mb-0">Total Revenue YTD</p>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
        <div class="col-md-4 col-sm-12 mb-4">
            <!-- Card -->
            <div class="card light-blue lighten-1 white-text">
                <!-- Card content -->
                <div class="card-body">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ __($total_signups_ytd) }}</p>
                        <p class="mb-0">Total Signups YTD</p>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
        <div class="col-md-4 col-sm-12 mb-4">
            <!-- Card -->
            <div class="card warning-color lighten-1 white-text">
                <!-- Card content -->
                <div class="card-body">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ __($new_signups) }}</p>
                        <p class="mb-0">New Signups (last 7 days)</p>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12 mb-4">
            <h4>Top {{ count($top_camps_by_revenue) }} Camps by $$$</h4>
            <ul class="list-group">
                @foreach ($top_camps_by_revenue as $itop_camps_by_revenue)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    {{ __($itop_camps_by_revenue->title) }}
                    <span class="badge badge-primary badge-pill">{{ money(__($itop_camps_by_revenue->price)) }}</span>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-6 col-sm-12 mb-4">
            <h4>Top {{ count($top_camps_by_registrations) }} Camps by Signups</h4>
            <ul class="list-group">
            @foreach ($top_camps_by_registrations as $itop_camps_by_registrations)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    {{ __($itop_camps_by_registrations->title) }}
                    <span class="badge badge-primary badge-pill">{{ __($itop_camps_by_registrations->total) }}</span>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
