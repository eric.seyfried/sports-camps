@extends('layouts.app')

@section('content')
<div class="container">
        <a href="{{ tenant_route('tenant:camp.create') }}" class="btn btn-success">Add Camp</a>
        <!-- Material checked -->
        <form action="{{ tenant_route('tenant:camp.list') }}" method="post" class="float-right" id="toggle_expired">
        @csrf
        <div class="switch">
            <!-- Default checked -->
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="include_expired" name="include_expired" value="true" {{ $include_expired  ? 'checked=checked' : ''}}>
                <label class="custom-control-label" for="include_expired">Include Expired Camps</label>
            </div>
        </div>
        </form>
    </div>
<div class="container">
    <div class="row justify-content-left">
    @foreach($camps as $i=>$camp)
        <div class="col-sm-4 mb-3">
            <div class="card" style="">
                <div class="view overlay success-color text-white pt-2 text-center" style="height:50px">
                    <h4 class="card-title">{{ elipsis(__($camp->title), 30) }}</h4>
                    <!-- <img class="card-img-top"
                    src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%28131%29.jpg" alt="Card image cap"> -->
                    <a>
                    <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <!-- <img class="card-img-top" src=".../100px180/" alt="Card image cap"> -->
                <div class="card-body">
                    <h4 class="card-title">{{ __(format_date($camp->camp_start_date,'M d Y')) }} - {{ __(format_date($camp->camp_end_date,'M d Y')) }}</h4>
                    <hr>
                    <h5 class="card-title">{{ elipsis(__($camp->summary)) }}</h5>
                    <p class="card-text">{{ elipsis(__($camp->description), 200) }}</p>
                    <a href="{{ tenant_route('tenant:camp.show',[$camp->id]) }}" class="btn btn-primary">View <i class="fas fa-arrow-circle-right pl-2"></i></a>
                </div>
                <div class="card-footer text-muted text-center">
                    Regular Price: {{ money(__($camp->regular_price)) }}
                </div>
            </div>
        </div>
    @endforeach
    </div>
    
</div>
@endsection

@section('pagespecificscripts')
<script>
$( document ).ready(function() {
    $('#include_expired').on('click', function(e){
        if ($(this).attr('checked')) {
            $(this).attr('checked', false);
        }
        else {
            $(this).attr('checked', true);
        }
        $('#toggle_expired').submit();
    });
});
</script>
@stop