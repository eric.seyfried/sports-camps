@extends('layouts.app')

@section('content')
<div class="container">
    <h3>View Camp</h3>
    <h5>{{ __($camp->title) }}</h5>

    @include('camp.partials.stats')

    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link{{ $active == 'summary' ? ' active' : '' }}" id="summary-tab" data-toggle="tab" href="#summary" role="tab" aria-controls="summary"
                    aria-selected="true">Summary</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ $active == 'edit' ? ' active' : '' }}" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit"
                    aria-selected="false">Edit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ $active == 'registrants' ? ' active' : '' }}" id="registrants-tab" data-toggle="tab" href="#registrants" role="tab" aria-controls="registrants"
                    aria-selected="false">Registrants</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ $active == 'campaign' ? ' active' : '' }}" id="campaign-tab" data-toggle="tab" href="#campaign" role="tab" aria-controls="campaign"
                    aria-selected="false">Campaign</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ $active == 'email' ? ' active' : '' }}" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email"
                    aria-selected="false">E-Mail</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane{{ $active == 'summary' ? ' active' : '' }}" id="summary" role="tabpanel" aria-labelledby="summary-tab">@include('camp.partials.summary')</div>
                <div class="tab-pane{{ $active == 'edit' ? ' active' : '' }}" id="edit" role="tabpanel" aria-labelledby="edit-tab">@include('camp.form')</div>
                <div class="tab-pane{{ $active == 'registrants' ? ' active' : '' }}" id="registrants" role="tabpanel" aria-labelledby="registrants-tab">@include('camp.partials.registrants')</div>
                <div class="tab-pane{{ $active == 'campaign' ? ' active' : '' }}" id="campaign" role="tabpanel" aria-labelledby="campaign-tab">@include('camp.partials.campaign')</div>
                <div class="tab-pane{{ $active == 'email' ? ' active' : '' }}" id="email" role="tabpanel" aria-labelledby="email-tab">@include('camp.partials.email')</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pagespecificscripts')
<script>
    $(document).ready(function(){
        $('.nav-link').on('click', function(e){
            var id = $(this).attr('href').replace('#','');
            history.pushState({
                    id: id
                }, 
                '', 
                '{{ tenant_route("tenant:camp.show",[$camp->id]) }}?active=' + id);
        });
    });

    history.replaceState({
                    id: 'foo'
                }, 
                '', 
                '{{ tenant_route("tenant:camp.show",[$camp->id]) }}?active={{ __($active) }}');

    
    window.addEventListener('popstate', function (event) {
        if (history.state && history.state.id === 'homepage') {
            // Render new content for the hompage
        }
    }, false);
</script>
@stop