@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Camp - {{ __($camp->title) }} <a href="{{ tenant_route('tenant:camp.delete',[$camp->id]) }}" id="camp-delete" class="btn btn-danger float-right">Delete</a></div>

                <div class="card-body">
                    @include('camp.form')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection