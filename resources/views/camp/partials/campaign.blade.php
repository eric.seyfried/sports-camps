<p class="alert alert-info">Instructional text here</p>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ tenant_route('tenant:camp.campaign-search') }}" id="camp-campaign-search-form">
            <input type="hidden" name="id" value="{{ $camp->id }}">
            <input type="hidden" name="active" value="{{ $active }}">
            @csrf

        <div class="form-row">
        <!-- gender -->
            <div class="md-form col-sm-12 col-md-2 mb-4">
                <!-- Default inline 1-->
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="gender-female" name="gender[]" value="female">
                    <label class="custom-control-label" for="gender-female">Female</label>
                </div>

                <!-- Default inline 2-->
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="gender-male" name="gender[]" value="male">
                    <label class="custom-control-label" for="gender-male">Male</label>
                </div>
            </div>
            <div class="md-form col-sm-12 col-md-2">
                <input type="number" id="age_min" class="form-control @error('age_min') is-invalid @enderror" name="age_min" value="{{ old('age_min') }}">
                <label for="age_min">{{ __('Age Min') }}</label>
                @error('age_min')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="md-form col-sm-12 col-md-2">
                <input type="number" id="age_max" class="form-control @error('age_max') is-invalid @enderror" name="age_max" value="{{ old('age_max') }}">
                <label for="age_max">{{ __('Age Max') }}</label>
                @error('age_max')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="md-form col-sm-12 col-md-3">
                <!-- Default unchecked -->
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="include-existing" name="include_existing" value="true">
                    <label class="custom-control-label" for="include-existing">Include Existing Registrants</label>
                </div>
            </div>
            <div class="md-form col-sm-12 col-md-3">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
        
        </form>
    </div>
</div>
@if (!is_null($leads))
<p class="alert alert-success">Your search found {{ __(count($leads)) }} results</p>
<form method="POST" action="{{ tenant_route('tenant:camp.campaign-save') }}" id="camp-campaign-form">
    <input type="hidden" name="id" value="{{ $camp->id }}">
    @csrf
    <div class="col-xs-12 col-md-12 table-responsive">
        <table class="table">
            <thead>
                <tr>
                <th scope="col"><input type="checkbox" class="form-control" id="check-all"></th>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">E-Mail</th>
                <th scope="col">Age</th>
                <th scope="col">Gender</th>
                <th scope="col">Existing Registrant</th>
                </tr>
            </thead>
            <tbody>
                @foreach($leads as $i => $lead)
                <tr class="">
                    <td><input type="checkbox" class="form-control" name="lead_id[]" value="{{ __($lead->id) }}"></td>
                    <th scope="row">{{ ($i+1) }}</th>
                    <td>{{ ucfirst(__($lead->first_name)) }}</td>
                    <td>{{ ucfirst(__($lead->last_name)) }}</td>
                    <td>{{ __($lead->email) }}</td>
                    <td>{{ __($lead->age()) }}</td>
                    <td>{{ ucfirst(__($lead->gender)) }}</td>
                    <td></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="md-form col-sm-12 col-md-3">
        <button type="submit" class="btn btn-primary">Create Campaign</button>
    </div>
</form>
@endif