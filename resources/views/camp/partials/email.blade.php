<div class="row">
    <div class="col-sm-12 col-md-12">
        <form method="POST" action="{{ tenant_route('tenant:camp.email', [$camp->id]) }}" id="camp-email-form">
                <input type="hidden" name="id" value="{{ $camp->id }}">
            @csrf
            @php
                $email_templates = ['early', 'late'];
            @endphp
            @foreach($email_templates as $i => $email_template_prefix)

            @php
                $prefix_start_date_field = $email_template_prefix . '_price_starts';
                $prefix_end_date_field = $email_template_prefix . '_price_ends';
                $email_template_type = $email_template_prefix . '_registration';
            @endphp
            <!-- Early Registration E-Mail -->
            <h5>{{ ucfirst(__($email_template_prefix)) }} Registration E-Mail</h5>
            <p>If your camp has an early registration price and an e-mail campaign set up, we'll send out promotional e-mails before the regitration period begins and ends.
                You can adjust the settings and e-mail template below.
            </p>
            <div class="form-row align-items-center mb-4">
                <div class="col-sm-12 col-md-1">
                    <div class="custom-control custom-checkbox mt-0">
                        <input type="checkbox" class="custom-control-input" id="enable_{{ __($email_template_prefix) }}_registration_email_starts" name="enable_{{ __($email_template_prefix) }}_registration_email_starts" {{ $camp->{$email_template_prefix.'_price_email_on'} ? 'checked' : '' }}>
                        <label class="custom-control-label" for="enable_{{ __($email_template_prefix) }}_registration_email_starts">Enable</label>
                    </div>
                </div>
            </div>
            <div class="form-row align-items-center mb-4">
                <div class="col-sm-12 col-md-11">
                    <div class="md-form input-group mt-0 mb-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text md-addon pl-0">Send E-Mail</span>
                        </div>
                        <input type="number" class="form-control col-md-1" name="{{ __($email_template_prefix) }}_price_email_start_days" aria-label="number of days to send email" value="{{ __($camp->{$email_template_prefix.'_price_email_start_days'}) }}">
                        <div class="input-group-append">
                            <span class="md-addon mt-2">days before {{ __($email_template_prefix) }} registration starts: {{ format_date(__($camp->{$prefix_start_date_field}), 'M d Y') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row align-items-center mb-4">
                <div class="col-sm-12 col-md-11">
                    <div class="md-form input-group mt-0 mb-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text md-addon pl-0">Send E-Mail</span>
                        </div>
                        <input type="number" class="form-control col-md-1" name="{{ __($email_template_prefix) }}_price_email_end_days" aria-label="number of days to send email" value="{{ __($camp->{$email_template_prefix.'_price_email_end_days'}) }}">
                        <div class="input-group-append">
                            <span class="md-addon mt-2">days before {{ __($email_template_prefix) }} registration ends: {{ format_date(__($camp->{$prefix_end_date_field}), 'M d Y') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            

            <!--Accordion wrapper {{ $i }}-->
            <div class="accordion md-accordion mb-4" id="accordion-wrapper-{{ $i }}" role="tablist" aria-multiselectable="true">
                    <!-- Accordion card -->
                    <div class="card">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="{{ __($email_template_prefix) }}_registration_email_template_header">
                        <a data-toggle="collapse" data-parent="#accordion-wrapper-{{ $i }}" href="#{{ __($email_template_prefix) }}_registration_email_template" aria-expanded="true" aria-controls="{{ __($email_template_prefix) }}_registration_email_template">
                        <h6 class="mb-0">
                        {{ ucfirst(__($email_template_prefix)) }} Registration Email Template <i class="fas fa-angle-down rotate-icon"></i>
                        </h6>
                        </a>
                    </div>

                    <!-- Card body -->
                    <div id="{{ __($email_template_prefix) }}_registration_email_template" class="collapse" role="tabpanel" aria-labelledby="{{ __($email_template_prefix) }}_registration_email_template" data-parent="#accordionEx">
                        <div class="card-body md-form">
                            <input type="hidden" name="{{ $email_template_prefix }}_registration_email_template_id" value="{{ __($camp->emailTemplates()->templateType($email_template_prefix.'_registration')->first()->id) }}" >
                            <!-- subject -->
                            <div class="md-form">
                                <input type="text" id="{{ __($email_template_prefix) }}_registration_email_subject" class="form-control @error($email_template_prefix.'_registration_email_subject') is-invalid @enderror" name="{{ __($email_template_prefix) }}_registration_email_subject" value="{{ old($email_template_prefix.'_registration_email_subject', $camp->emailTemplates()->templateType($email_template_type)->first()->subject) }}" required>
                                <label for="">{{ __('Subject') }}</label>
                                @error('{{ __($email_template_prefix) }}_registration_email_subject')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <!-- body -->
                            <div class="md-form">
                                <textarea name="{{ __($email_template_prefix) }}_registration_email_body" class="md-textarea form-control @error($email_template_prefix.'_registration_email_body') is-invalid @enderror" rows="6">{{ __($camp->emailTemplates()->templateType($email_template_type)->first()->body) }}</textarea>
                                <label for="form7">Body</label>
                            </div>
                            <button type="button" class="btn btn-outline-info waves-effect" data-toggle="modal" data-target="#modal-email-help">
                                Help
                            </button>
                        </div>
                    </div>
                    <!-- Accordion card -->
                </div>
            </div>
            @endforeach


           
            <button type="submit" class="btn btn-primary">Save Settings</button>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-email-help" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">E-Mail Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="alert alert-info" role="alert">You may use the following variables in your e-mail: </p>
        <ul>
            <li>{camp_title}</li>
            <li>{camp_start_date}</li>
            <li>{camp_end_date}</li>
            <li>{camp_start_time}</li>
            <li>{camp_end_time}</li>
            <li>{regular_price}</li>
            <li>{early_price}</li>
            <li>{early_price_starts}</li>
            <li>{early_price_ends}</li>
            <li>{late_price}</li>
            <li>{late_price_starts}</li>
            <li>{late_price_ends}</li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>