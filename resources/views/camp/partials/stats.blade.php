<!-- Section: Block Content -->
<section class="sc-stats mt-2">
    <!-- Grid row -->
    <div class="row">
        <!-- Grid column -->
        <div class="col-md-6 col-lg-3 mb-4">
            <!-- Card -->
            <div class="card success-color white-text">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ money(__($camp->totalRevenue())) }}</p>
                        <p class="mb-0">Revenue</p>
                    </div>
                    <div>
                        <i class="fas fa-money-bill-wave fa-3x text-black-40"></i>
                    </div>
                </div>
                <a class="card-footer footer-hover small text-center white-text border-0 p-2" href="{{ tenant_route('tenant:camp.show',[$camp->id,'filter'=>'revenue']) }}">More info<i class="fas fa-arrow-circle-right pl-2"></i></a>
            </div>
            <!-- Card -->
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-3 mb-4">
            <!-- Card -->
            <div class="card warning-color white-text">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ money(__($camp->totalRefunds())) }}</p>
                        <p class="mb-0">Refunds</p>
                    </div>
                    <div>
                        <i class="fas fa-money-bill-wave fa-3x text-black-40"></i>
                    </div>
                </div>
                <a class="card-footer footer-hover small text-center white-text border-0 p-2" href="{{ tenant_route('tenant:camp.show',[$camp->id,'filter'=>'refunds']) }}">More info<i class="fas fa-arrow-circle-right pl-2"></i></a>
            </div>
            <!-- Card -->
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-3 mb-4">
            <!-- Card -->
            <div class="card light-blue lighten-1 white-text">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ __($camp->totalRegistrations()) }}</p>
                        <p class="mb-0">Registrations</p>
                    </div>
                    <div>
                        <i class="fas fa-user-plus fa-3x text-black-40"></i>
                    </div>
                </div>  
                <a class="card-footer footer-hover small text-center white-text border-0 p-2" href="{{ tenant_route('tenant:camp.show',[$camp->id,'filter'=>'registrations']) }}">More info<i class="fas fa-arrow-circle-right pl-2"></i></a>     
            </div>
            <!-- Card -->
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-3 mb-4">
            <!-- Card -->
            <div class="card red accent-2 white-text">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <p class="h2-responsive font-weight-bold mt-n2 mb-0">{{ __($camp->totalCancelled()) }}</p>
                        <p class="mb-0">Cancellations</p>
                    </div>
                    <div>
                        <i class="fas fa-user-minus fa-3x text-black-40"></i>
                    </div>
                </div>
                <a class="card-footer footer-hover small text-center white-text border-0 p-2" href="{{ tenant_route('tenant:camp.show',[$camp->id,'filter'=>'cancellations']) }}">More info<i class="fas fa-arrow-circle-right pl-2"></i></a>
            </div>
            <!-- Card -->
        </div>
        <!-- Grid column -->
    </div>
    <!-- Grid row -->
</section>
<!-- Section: Block Content -->