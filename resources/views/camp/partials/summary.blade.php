<p>{{ format_date(__($camp->camp_start_date), 'M d Y') }} - {{ format_date(__($camp->camp_end_date), 'M d Y') }}</p>
<p>{{ __($camp->summary) }}</p>
<p>{{ __($camp->description) }}</p>
<div class="row">
    <div class="col-sm-12 col-md-6">
        <h5>Pricing</h5>
        <hr>
        <dl class="row">
            <dt class="col-md-6 col-sm-12">Regular Price<dt>
            <dd class="col-md-6 col-sm-12">{{ money(__($camp->regular_price)) }}</dd>
            <dt class="col-md-6 col-sm-12">Early Registration Price
                <p><small><strong>Starts</strong> {{ __($camp->early_price_starts) }} <strong>Ends</strong> {{ __($camp->early_price_ends) }}</small></p>
            <dt>
            <dd class="col-md-6 col-sm-12"><p>{{ money(__($camp->early_price)) }}</p></dd>
            <dt class="col-md-6 col-sm-12">Late Registration Price
                <p><small><strong>Starts</strong> {{ __($camp->late_price_starts) }} <strong>Ends</strong> {{ __($camp->late_price_ends) }}</small></p>
            <dt>
            <dd class="col-md-6 col-sm-12"><p>{{ money(__($camp->late_price)) }}</p></dd>
        </dl>
    </div>
    <div class="col-sm-12 col-md-6">
        <h5>Location</h5>
        <hr>
        <p>{{ __($camp->address_1) }}</p>
        <p>{{ __($camp->address_2) }}</p>
        <p>{{ __($camp->city) }}, {{ __($camp->state) }} {{ __($camp->zip) }}</p>
    </div>
</div>