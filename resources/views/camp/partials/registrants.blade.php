<div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Email</th>
        <th scope="col">Status</th>
        <th scope="col">Date Registered</th>
        <th scope="col text-right">Price</th>
      </tr>
    </thead>
    <tbody>
    @foreach($camp->registration()->get() as $i=>$registration)
      @if(
        (in_array($filter, ['revenue', 'registrations']) && in_array($registration->status, ['cancelled'])) ||
        (in_array($filter, ['refunds', 'cancellations']) && in_array($registration->status, ['pending', 'paid']))
        )
        @php
          continue;
        @endphp
      @endif
      <tr>
        <th scope="row">{{ ($i+1) }}</th>
        <td>{{ __($registration->user->name) }}</td>
        <td>{{ __($registration->user->lastname) }}</td>
        <td>{{ __($registration->user->email) }}</td>
        <td>{{ ucfirst(__($registration->status)) }}</td>
        <td class="text-right">{{ __($registration->created_at)}}</td>
        <td class="text-right">{{ money(__($registration->price)) }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>