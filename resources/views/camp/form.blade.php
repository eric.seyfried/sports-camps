<form method="POST" action="{{ $camp->id ? tenant_route('tenant:camp.update') : tenant_route('tenant:camp.store') }}" id="camp-form">
    @if($camp->id)
        <input type="hidden" name="id" value="{{ $camp->id }}">
    @endif
    @csrf

    <!-- camp title -->
    <div class="md-form">
        <input type="text" id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title', $camp->title) }}" required autocomplete="title" autofocus>
        <label for="title">{{ __('Title') }}</label>
        @error('title')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <!-- camp summary -->
    <div class="md-form">
        <input type="text" id="summary" class="form-control @error('summary') is-invalid @enderror" name="summary" value="{{ old('summary', $camp->summary) }}" required autocomplete="summary" autofocus>
        <label for="summary">{{ __('Summary') }}</label>
        @error('summary')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <!-- camp description -->
    <div class="md-form">
        <textarea id="description" required class="md-textarea form-control @error('description') is-invalid @enderror" rows="3" name="description">{{ old('description', $camp->description) }}</textarea>
        <label for="description">{{ __('Description') }}</label>
        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-row">
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp camp_start_date -->
            <div class="md-form">
                <input type="text" id="camp_start_date" class="form-control @error('camp_start_date') is-invalid @enderror" name="camp_start_date" value="{{ old('camp_start_date', $camp->camp_start_date) }}" required autocomplete="camp_start_date" autofocus>
                <label for="camp_start_date">{{ __('Start Date') }}</label>
                @error('camp_start_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp camp_start_time -->
            <div class="md-form">
                <input type="text" id="camp_start_time" class="form-control @error('camp_start_time') is-invalid @enderror" name="camp_start_time" value="{{ old('camp_start_time', $camp->camp_start_time) }}" required autocomplete="camp_start_time" autofocus>
                <label for="camp_start_time">{{ __('Start Time') }}</label>
                @error('camp_start_time')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp camp_end_date -->
            <div class="md-form">
                <input type="text" id="camp_end_date" class="form-control @error('camp_end_date') is-invalid @enderror" name="camp_end_date" value="{{ old('camp_end_date', $camp->camp_end_date) }}" required autocomplete="camp_end_date" autofocus>
                <label for="camp_end_date">{{ __('End Date') }}</label>
                @error('camp_end_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp camp_end_time -->
            <div class="md-form">
                <input type="text" id="camp_end_time" class="form-control @error('camp_end_time') is-invalid @enderror" name="camp_end_time" value="{{ old('camp_end_time', $camp->camp_end_time) }}" required autocomplete="camp_end_time" autofocus>
                <label for="camp_end_time">{{ __('End Time') }}</label>
                @error('camp_end_time')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>

    <!-- camp regular_price -->
    <div class="md-form input-with-pre-icon">
        <i class="fas fa-dollar-sign input-prefix"></i>
        <input type="text" id="regular_price" class="form-control @error('regular_price') is-invalid @enderror" name="regular_price" value="{{ old('regular_price', $camp->regular_price) }}" required autocomplete="regular_price" autofocus>
        <label for="regular_price">{{ __('Regular Price') }}</label>
        @error('regular_price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <h5>Early Registration</h5>
    <!-- camp early_price -->
    <div class="md-form input-with-pre-icon">
        <i class="fas fa-dollar-sign input-prefix"></i>
        <input type="text" id="early_price" class="form-control @error('early_price') is-invalid @enderror" name="early_price" value="{{ old('early_price', $camp->early_price) }}" required autocomplete="early_price" autofocus>
        <label for="early_price">{{ __('Early Registation Price') }}</label>
        @error('early_price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-row">
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp early_price_starts -->
            <div class="md-form">
                <input type="text" id="early_price_starts" class="form-control @error('early_price_starts') is-invalid @enderror" name="early_price_starts" value="{{ old('early_price_starts', $camp->early_price_starts) }}" required autocomplete="early_price_starts" autofocus>
                <label for="early_price_starts">{{ __('Early Registation Start Date') }}</label>
                @error('early_price_starts')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp early_price_ends -->
            <div class="md-form">
                <input type="text" id="early_price_ends" class="form-control @error('early_price_ends') is-invalid @enderror" name="early_price_ends" value="{{ old('early_price_ends', $camp->early_price_ends) }}" required autocomplete="early_price_ends" autofocus>
                <label for="early_price_ends">{{ __('Early Registation End Date') }}</label>
                @error('early_price_ends')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>

    <h5>Late Registration</h5>
    <!-- camp late_price -->
    <div class="md-form input-with-pre-icon">
        <i class="fas fa-dollar-sign input-prefix"></i>
        <input type="text" id="late_price" class="form-control @error('late_price') is-invalid @enderror" name="late_price" value="{{ old('late_price', $camp->late_price) }}" required autocomplete="late_price" autofocus>
        <label for="late_price">{{ __('Late Registration Price') }}</label>
        @error('late_price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-row">
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp late_price_starts -->
            <div class="md-form">
                <input type="text" id="late_price_starts" class="form-control @error('late_price_starts') is-invalid @enderror" name="late_price_starts" value="{{ old('late_price_starts', $camp->late_price_starts) }}" required autocomplete="late_price_starts" autofocus>
                <label for="late_price_starts">{{ __('Late Registration Start Date') }}</label>
                @error('late_price_starts')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <!-- camp late_price_ends -->
            <div class="md-form">
                <input type="text" id="late_price_ends" class="form-control @error('late_price_ends') is-invalid @enderror" name="late_price_ends" value="{{ old('late_price_ends', $camp->late_price_ends) }}" required autocomplete="late_price_ends" autofocus>
                <label for="late_price_ends">{{ __('Late Registration End Date') }}</label>
                @error('late_price_ends')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
  
    <h5>Camp Location</h5>
    <!-- camp address_1 -->
    <div class="md-form">
        <input type="text" id="address_1" class="form-control @error('address_1') is-invalid @enderror" name="address_1" value="{{ old('address_1', $camp->address_1) }}" required autocomplete="address_1" autofocus>
        <label for="address_1">{{ __('Address') }}</label>
        @error('address_1')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <!-- camp address_2 -->
    <div class="md-form">
        <input type="text" id="address_2" class="form-control @error('address_2') is-invalid @enderror" name="address_2" value="{{ old('address_2', $camp->address_2) }}" required autocomplete="address_2" autofocus>
        <!-- <label for="address_2">{{ __('Address') }}</label> -->
        @error('address_2')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-row">
        <div class="col-lg-4 col-xs-12 col-sm-4">
            <!-- camp city -->
            <div class="md-form">
                <input type="text" id="city" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city', $camp->city) }}" required autocomplete="city" autofocus>
                <label for="city">{{ __('City') }}</label>
                @error('city')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 col-sm-4">
            <!-- camp state -->
            <div class="md-form">
                <input type="text" id="state" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ old('state', $camp->state) }}" required autocomplete="state" autofocus>
                <label for="state">{{ __('State') }}</label>
                @error('state')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 col-sm-4">
            <!-- camp zip -->
            <div class="md-form">
                <input type="text" id="zip" class="form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip', $camp->zip) }}" required autocomplete="zip" autofocus>
                <label for="zip">{{ __('Zip') }}</label>
                @error('zip')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-12">
            <button type="submit" class="btn btn-success btn-lg btn-block" id="submit">
                Save Camp
            </button>
        </div>
    </div>
</form>