@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header info-color white-text text-center py-4">
                    <strong>Purchase {{ __($plan->name) }} Plan</strong>
                </h5>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id="payment-form">
                    <input type="hidden" name="plan" value="{{ __($plan->id) }}" />
                    <input type="hidden" name="tenant_id" id="tenant_id" value="{{ __($tenant->id ?? '') }}" />
                        @csrf

                        <!-- First Name -->
                        <div class="md-form">
                            <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <label for="name">{{ __('First Name') }}</label>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Last Name -->
                        <div class="md-form">
                            <input type="text" id="lastname" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                            <label for="lastname">{{ __('Last Name') }}</label>
                            @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Company -->
                        <div class="md-form">
                            <input type="text" id="tenant_name" class="form-control @error('tenant_name') is-invalid @enderror" name="tenant_name" value="{{ old('tenant_name') }}" required autocomplete="tenant_name" autofocus>
                            <label for="tenant_name">{{ __('Company Name') }}</label>
                            @error('tenant_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Email -->
                        <div class="md-form">
                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <label for="email">{{ __('E-Mail') }}</label>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Password -->
                        <div class="md-form">
                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            <label for="password">{{ __('Password') }}</label>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- Confirm Password -->
                        <div class="md-form">
                            <input type="password" id="password-confirm" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                        </div>
                        @include('partials.stripeform')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('partials.ccscript')