@extends('layouts.app')
@php
    $card_colors = ['blue', 'green', 'indigo'];
@endphp
@section('content')
<div class="container">
    <!-- Grid row -->
    <div class="row justify-content-center">
    @foreach ($plans as $i => $plan)
      <!-- Grid column -->
      <div class="col-lg-4 col-md-12 mb-4">
        <!-- Pricing card -->
        <form method="get" action="{{ route('register') }}">
            @csrf
        <div class="card pricing-card">
          <!-- Price -->
          <div class="price header white-text {{ $card_colors[$i]}} rounded-top">
            <h2 class="number">{{ $plan->amount }}</h2>
            <div class="version">
              <h5 class="mb-0">{{ __($plan->name) }}</h5>
            </div>
          </div>
          <!-- Features -->
          <div class="card-body striped mb-1">
            <ul>
              <li>
                <p class="mt-2"><i class="fas fa-check green-text pr-2"></i>Feature 1</p>
              </li>
              <li>
                <p><i class="fas fa-check green-text pr-2"></i>Feature 2</p>
              </li>
              <li>
                <p><i class="fas fa-times red-text pr-2"></i>Feature 3</p>
              </li>
              <li>
                <p><i class="fas fa-times red-text pr-2"></i>Feature 4</p>
              </li>
              <li>
                <p><i class="fas fa-times red-text pr-2"></i>Feature 5</p>
              </li>
            </ul>
            <button class="btn btn-blue" type="submit" name="plan" value="{{ __($plan->id) }}">Buy {{ __($plan->name) }}</button>
          </div>
          <!-- Features -->
        </div>
        </form>
        <!-- Pricing card -->
      </div>
      <!-- Grid column -->
      @endforeach
    </div>
    <!-- Grid row -->
</div>
@endsection