<nav class="mb-1 navbar navbar-expand-lg navbar-dark primary-color-dark">
    <a class="navbar-brand" href="{{ url('/') }}">Sports Camp Pro</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-3" aria-controls="navbarSupportedContent-3" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-3">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item{{ (Request::segment(2) == 'home' || Request::segment(2) == 'dashboard') ? ' active' : '' }}">
            <a class="nav-link waves-effect waves-light" href="{{ tenant_route('tenant:dashboard') }}">Dashboard
            </a>
        </li>
        <li class="nav-item{{ (Request::segment(2) == 'camp') ? ' active' : '' }}">
            <a class="nav-link waves-effect waves-light" href="{{ tenant_route('tenant:camp.list') }}">Camps</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown
            </a>
            <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-3">
            <a class="dropdown-item waves-effect waves-light" href="#">Action</a>
            <a class="dropdown-item waves-effect waves-light" href="#">Another action</a>
            <a class="dropdown-item waves-effect waves-light" href="#">Something else here</a>
            </div>
        </li>
        </ul>
        <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fas fa-user"></i> {{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item waves-effect waves-light" href="#">Settings</a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        </ul>
    </div>
</nav>