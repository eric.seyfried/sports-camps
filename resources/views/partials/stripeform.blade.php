<h5 class="card-title text-md-left">Payment Information</h5>
<!-- Cardholder Name -->
<div class="md-form">
    <input type="text" id="card-holder-name" class="form-control" name="card_holder_name" required>
    <label for="card-holder-name">{{ __('Cardholder Name') }}</label>
</div>

<div class="form-group row">
    <div class="col-md-6" id="card-element">
    <!-- A Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors. -->
    <div id="card-errors" role="alert"></div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-12">
        <button type="submit" class="btn btn-blue btn-block my-4 waves-effect z-depth-0 btn-lg btn-block" id="card-button" data-secret="{{ $intent->client_secret }}">
            Purchase {{ __($name) }} @ {{ __('$' . number_format($amount,2)) }}
        </button>
    </div>
</div>