
@section('pagespecificscripts')
<script src="https://js.stripe.com/v3/"></script>
    <script>
    $( document ).ready(function() {
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        //4242424242424242


        
        var stripe = Stripe('{{ env("STRIPE_KEY") }}');
        
        /*  cashier method */
        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');

        const cardHolderName = document.getElementById('card-holder-name');
        const cardButton = document.getElementById('card-button');
        const clientSecret = cardButton.dataset.secret;
        var form = document.getElementById('payment-form');
        var isOneTimeCharge = (document.getElementById('tenant_id').value != '') ? true : false;

        form.addEventListener('submit', async (e) => {
            event.preventDefault();
            if (!isOneTimeCharge) {
                const { setupIntent, error } = await stripe.handleCardSetup(
                    clientSecret, cardElement, {
                        payment_method_data: {
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );
                payment_method_token = setupIntent.payment_method;
                if (error) {
                    // Display "error.message" to the user...
                } else {
                    // The card has been verified successfully...
                    stripeTokenHandler(payment_method_token);
                }
            }
            else {
                const { paymentMethod, error } = await stripe.createPaymentMethod(
                    'card', cardElement, {
                        billing_details: { name: cardHolderName.value }
                });
                payment_method_token = paymentMethod.id;
                if (error) {
                    // Display "error.message" to the user...
                } else {
                    // The card has been verified successfully...
                    stripeTokenHandler(payment_method_token);
                }
            }
            
            
        });

        
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
        /*  end cashier method */


    });

    </script>
@stop